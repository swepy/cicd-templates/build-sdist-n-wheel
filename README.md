# Build SDist and Wheel template

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://lab.frogg.it/swepy/cicd-templates/build-sdist-n-wheel/-/blob/main/LICENSE)

## Objective

Run a job to build a source distribution and a wheel package for a Python project.

## How to use it

1. Include the build_sdist_n_wheel template in your GitLab CI/CD configuration.
2. If you need to customize the job, refer to
   the [jobs customization](https://docs.r2devops.io/get-started/use-templates/#job-templates-customization)
   documentation.

## Variables

| Name                  | Description                                                      | Default                    |
|-----------------------|------------------------------------------------------------------|----------------------------|
| `IMAGE_NAME`          | The default name for the docker image.                           | `"python"`                 |
| `IMAGE_TAG`           | The default tag for the docker image.                            | `"latest"`                 |
| `IMAGE`               | The default docker image name.                                   | `"$IMAGE_NAME:$IMAGE_TAG"` |
| `PROJECT_PATH`        | The path to the project root directory.                          | `"."`                      |
| `BUILD_SDIST_N_WHEEL` | The command to build the source distribution and wheel packages. | `"python -m build"`        |
