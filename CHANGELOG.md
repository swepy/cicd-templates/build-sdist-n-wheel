# Changelog

All notable changes to this job will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0] - 2024-07-16

### Changed

* Use gitlab component repository layout
* Version tags now follow strict SemVers

## [0.1.3] - 2024-05-10

### Changed

* build-sdist-n-wheel now uses the `build` stage

## [0.1.2] - 2024-05-08

### Changed

* By default, the pipeline will now run only on tags

## [0.1.1] - 2024-05-08

### Fixed

* Template was not working as expected because of a misconfiguration of the virtual 
  environment.

## [0.1.0] - 2024-05-07

* Initial version
